# Changelog

## Version 1.2.0

Added:

- Support for Number Portability v3; adds the contract field to porting requests
