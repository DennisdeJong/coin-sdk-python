# Python Number Portability SDK

## Introduction

This SDK supports secured access to the number portability API.

For a quick start, follow the steps below:
* [Setup](#setup)
* [Configure Credentials](#configure-credentials)
* [Send Messages](#send-messages)
* [Consume Messages](#consume-messages)


## Setup

### Sample Project for the Number Portability API
- A sample project is provided in the `samples/number_portability` directory.


## Configure Credentials

For secure access credentials are required.
- Check the [README introduction](../../README.md#introduction) to find out how to configure these.
- In summary you will need:
    - a consumer name
    - a private key file `private-key.pem`
    - a file containing the encrypted Hmac secret `sharedkey.encrypted`

Configure these setting at the bottom of the quickstart.py file: 

## Send Messages
`quickstart.py` shows how to create and send messages. 

When successful (HTTP 200), the `send()` function returns a `MessageResponse` object:
```python
class MessageResponse:
    transaction_id: str
```
When unsuccessful (HTTP != 200), the `send()` function will throw a `requests.HTTPError`.
When applicable, this error contains the error code and description returned by the API.


## Consume Messages
### <a name="listener"></a>Create Message Listener
For message consumption the number portability API makes use of http's [Server-Sent Events](https://en.wikipedia.org/wiki/Server-sent_events).
The SDK provides a receiver class that takes care of messages incoming from the event stream.
This class also provides the possibility to act upon the various porting messages that are streamed.
Whenever the API doesn't send any other message for 20 seconds, it sends an empty 'heartbeat' message, which triggers the on_keep_alive() method.

To use the receiver, you will have to extend it and implement the abstract methods.
Again, `quickstart.py` shows an example of how this can be done.


### <a name="consumer"></a>Start consuming Messages 
By default the Receiver consumes <strong>all</strong> <strong>Unconfirmed</strong> messages. 


### <a name="filter"></a>Consume specific messages using filters

The `NumberPortabilityMessageConsumer` provides various ways / filters how messages can be consumed. The filters are:
- `MessageType`: All possible message types including errors.
- ConfirmationStatus: 
    - `ConfirmationStatus.UNCONFIRMED`: consumed all unconfirmed messages. Upon (re)-connection all unconfirmed messages are served.
    - `ConfirmationStatus.ALL`: consumes confirmed and unconfirmed messages. <strong>Note:</strong> this filter enables the consumption of the *whole message history*. Therefore, this filter can best be used in conjunction with the `offset`. 
- `offset`: starts consuming messages based on the given `message-id` offset. When using `ConfirmationStatus.UNCONFIRMED` the `offset` is (in most cases) of no use. Using the `ConfirmationStatus.ALL` filter it can be valuable. <strong>Note:</strong> it is the responsibility of the client to keep track of the `offset`.

#### <a name="ex1"></a>Consumer Filter Example 1: filter on MessageTypes
```python
# only listen for portingperformed messages
receiver.start_stream(confirmation_status = ConfirmationStatus.UNCONFIRMED, message_types = [MessageType.PORTING_PERFORMED_V3]) 
```

#### <a name="ex2"></a>Consumer Filter Example 2: filter on offset
```
# get all messages from offset 1234
receiver.start_stream(offset = 1234, confirmation_status = ConfirmationStatus.ALL) 
```
The `message`-input variable is a `namedtuple`, so the `message` properties can be accessed object-wise:
```python
# example
networkoperator = message.header.receiver.networkoperator
dossierid = message.body.portingrequest.dossierid
```
Property names are equal to the property names in the event stream JSON.
See the [Swagger File](https://test-api.coin.nl/docs/number-portability/v3/swagger.json) for JSON definitions of the all messages.
