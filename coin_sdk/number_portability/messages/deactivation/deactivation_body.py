# coding: utf-8

"""
    COIN Number Portability API V3

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)

    OpenAPI spec version: 3.0.0
    Contact: servicedesk@coin.nl
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint

import six


class DeactivationBody(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'deactivation': 'Deactivation'
    }

    attribute_map = {
        'deactivation': 'deactivation'
    }

    def __init__(self, deactivation=None):
        """DeactivationBody - a model defined in Swagger"""
        self._deactivation = None
        self.discriminator = None
        self.deactivation = deactivation

    @property
    def deactivation(self):
        """Gets the deactivation of this DeactivationBody.


        :return: The deactivation of this DeactivationBody.
        :rtype: Deactivation
        """
        return self._deactivation

    @deactivation.setter
    def deactivation(self, deactivation):
        """Sets the deactivation of this DeactivationBody.


        :param deactivation: The deactivation of this DeactivationBody.
        :type: Deactivation
        """
        if deactivation is None:
            raise ValueError("Invalid value for `deactivation`, must not be `None`")

        self._deactivation = deactivation

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(DeactivationBody, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, DeactivationBody):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
