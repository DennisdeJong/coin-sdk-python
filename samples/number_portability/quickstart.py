import random
import logging

from coin_sdk.number_portability.messages.cancel import CancelBuilder
from coin_sdk.number_portability.messages.deactivation import DeactivationBuilder
from coin_sdk.number_portability.messages.deactivationservicenumber.deactivation_service_number_builder import \
    DeactivationServiceNumberBuilder
from coin_sdk.number_portability.messages.enum import EnumActivationNumberBuilder, EnumDeactivationNumberBuilder, \
    EnumDeactivationRangeBuilder, EnumActivationRangeBuilder, EnumProfileActivationBuilder, \
    EnumProfileDeactivationBuilder
from coin_sdk.number_portability.messages.enum.enum_activation_operator_builder import EnumActivationOperatorBuilder
from coin_sdk.number_portability.messages.enum.enum_deactivation_operator_builder import EnumDeactivationOperatorBuilder
from coin_sdk.number_portability.messages.portingperformed import PortingPerformedBuilder
from coin_sdk.number_portability.messages.portingrequest import PortingRequestBuilder
from coin_sdk.number_portability.messages.portingrequest.porting_request_builder import ContractType
from coin_sdk.number_portability.messages.portingrequestanswer import PortingRequestAnswerBuilder
from coin_sdk.number_portability.messages.portingrequestanswerdelayed import PortingRequestAnswerDelayedBuilder
from coin_sdk.number_portability.messages.activationservicenumber import ActivationServiceNumberBuilder
from coin_sdk.number_portability.messages.tariffchangeservicenumber.tariff_change_service_number_builder import \
    TariffChangeServiceNumberBuilder
from coin_sdk.number_portability.receiver import Receiver
from coin_sdk.number_portability.sender import Sender
from coin_sdk.number_portability.npconfig import NpConfig, set_logging


class QuickstartSender:
    operator = None

    def __init__(self, config, operator):
        self._config = config
        self.operator = operator
        self._sender = Sender(self._config)

    def porting_request(self, dossier_id: str, number_start: str, number_end: str):
        porting_request = (
            PortingRequestBuilder()
            .set_dossierid(dossier_id)
            .set_recipientnetworkoperator(self.operator)
            .set_recipientserviceprovider(self.operator)
            .set_contract(ContractType.CONTINUATION)
            .set_header(sender_network_operator=self.operator, receiver_network_operator='CRDB', sender_service_provider=self.operator)

            .add_porting_request_seq()
                .set_number_series(number_start, number_end)
                .finish()
                .set_customerinfo(None, "Vereniging COIN", "10", None, "2803PK", "1234")
                .build()
        )
        print(porting_request)
        self._sender.send_message(porting_request)

    def porting_request_answer(self, dossier_id: str, number_start: str, number_end: str, receiver: str):
        porting_performed_answer = (
            PortingRequestAnswerBuilder()
            .set_header(self.operator, receiver, self.operator, receiver)
            .set_dossierid(dossier_id)
            .set_blocking('N')
            .add_porting_request_answer_seq()
                .set_donornetworkoperator(self.operator)
                .set_donorserviceprovider(self.operator)
                .set_firstpossibledate('20190101120000')
                .set_number_series(number_start, number_end)
                .finish()
            .build()
        )
        print(porting_performed_answer)
        self._sender.send_message(porting_performed_answer)

    def cancel(self, dossier_id: str, receiver: str):
        cancel = CancelBuilder() \
            .set_dossierid(dossier_id) \
            .set_header(sender_network_operator=self.operator, receiver_network_operator=receiver) \
            .build()
        self._sender.send_message(cancel)

    def deactivation(self, dossier_id: str, number_start: str, number_end: str, receiver: str):
        deactivation = (
            DeactivationBuilder()
            .set_header(sender_network_operator=self.operator, receiver_network_operator=receiver)
            .set_dossierid(dossier_id)
            .set_currentnetworkoperator(self.operator)
            .set_originalnetworkoperator(receiver)
            .add_deactivation_seq()
                .set_number_series(number_start, number_end)
                .finish()
                .build()
        )
        print(deactivation)
        self._sender.send_message(deactivation)

    def porting_performed(self, dossier_id: str, number_start: str, number_end: str, donor: str):
        porting_performed = (
            PortingPerformedBuilder()
            .set_header(sender_network_operator=self.operator, receiver_network_operator='ALLO')
            .set_dossierid(dossier_id)
            .set_donornetworkoperator(donor)
            .set_recipientnetworkoperator(self.operator)
            .add_porting_performed_seq()
                .set_number_series(number_start, number_end)
                .finish()
                .build()
        )
        print(porting_performed)
        self._sender.send_message(porting_performed)

    def porting_request_answer_delayed(self, dossier_id: str, receiver: str):
        porting_request_answer_delayed = (
            PortingRequestAnswerDelayedBuilder()
            .set_header(self.operator, receiver, self.operator, receiver)
            .set_dossierid(dossier_id)
            .set_donornetworkoperator(self.operator)
            .build()
        )
        self._sender.send_message(porting_request_answer_delayed)

    def activationsn(self, dossier_id: str, number_start: str, number_end: str, receiver: str):
        activationsn = (
            ActivationServiceNumberBuilder()
            .set_header(sender_network_operator=self.operator, receiver_network_operator=receiver)
            .set_dossierid(dossier_id)
            .set_platformprovider(self.operator)
            .set_planneddatetime("20190101121212")
            .set_note("A Note")
            .add_activationServiceNumber_seq()
                .set_number_series(number_start, number_end)
                .set_tariff_info("1023,00", "1023,00", "1", "2", "3")
                .set_pop("Test")
                .finish()
            .build()
        )
        print(activationsn)
        self._sender.send_message(activationsn)

    def deactivationsn(self, dossier_id: str, number_start: str, number_end: str, receiver: str):
        deactivationsn = (
            DeactivationServiceNumberBuilder()
            .set_header(sender_network_operator=self.operator, receiver_network_operator=receiver)
            .set_dossierid(dossier_id)
            .set_platformprovider(self.operator)
            .set_planneddatetime("20190101121212")
            .add_deactivationServiceNumber_seq()
                .set_number_series(number_start, number_end)
                .set_pop("Test")
                .finish()
            .build()
        )
        print(deactivationsn)
        self._sender.send_message(deactivationsn)

    def tariffchangesn(self, dossier_id: str, number_start: str, number_end: str, receiver: str):
        tariffchangesn = (
            TariffChangeServiceNumberBuilder()
            .set_header(sender_network_operator=self.operator, receiver_network_operator=receiver)
            .set_dossierid(dossier_id)
            .set_platformprovider(self.operator)
            .set_planneddatetime("20190101121212")
            .add_tariffChangeServiceNumber_seq()
                .set_number_series(number_start, number_end)
                .set_tariff_info("1023,00", "1023,00", "1", "2", "3")
                .finish()
            .build()
        )
        print(tariffchangesn)
        self._sender.send_message(tariffchangesn)

    def enumactivationnumber(self, dossier_id: str, number_start: str, number_end: str, receiver: str):
        enumactivationnumber = (
            EnumActivationNumberBuilder()
            .set_header(sender_network_operator=self.operator, receiver_network_operator=receiver)
            .set_dossierid(dossier_id)
            .set_currentnetworkoperator('TST02')
            .set_typeofnumber("1")
            .add_enum_activation_number_seq()
                .set_number_series(number_start, number_end)
                .add_enum_profiles("PROF1", "PROF2")
                .finish()
            .build()
        )
        print(enumactivationnumber)
        self._sender.send_message(enumactivationnumber)

    def enumdeactivationnumber(self, dossier_id: str, number_start: str, number_end: str, receiver: str):
        enumdeactivationnumber = (
            EnumDeactivationNumberBuilder()
            .set_header(sender_network_operator=self.operator, receiver_network_operator=receiver)
            .set_dossierid(dossier_id)
            .set_currentnetworkoperator('TST02')
            .set_typeofnumber("1")
            .add_enum_deactivation_number_seq()
                .set_number_series(number_start, number_end)
                .add_enum_profiles("PROF1", "PROF2")
                .finish()
            .build()
        )
        print(enumdeactivationnumber)
        self._sender.send_message(enumdeactivationnumber)

    def enumactivationoperator(self, dossier_id: str, receiver: str):
        enumactivationoperator = (
            EnumActivationOperatorBuilder()
            .set_header(sender_network_operator=self.operator, receiver_network_operator=receiver)
            .set_dossierid(dossier_id)
            .set_currentnetworkoperator('TST02')
            .set_typeofnumber("1")
            .add_enum_activation_operator_seq()
                .set_profileid("PROF-12")
                .set_default_service("Y")
                .finish()
            .add_enum_activation_operator_seq()
                .set_profileid("PROF-32")
                .set_default_service("Y")
                .finish()
            .build()
        )
        print(enumactivationoperator)
        self._sender.send_message(enumactivationoperator)

    def enumdeactivationoperator(self, dossier_id: str, receiver: str):
        enumdeactivationoperator = (
            EnumDeactivationOperatorBuilder()
            .set_header(sender_network_operator=self.operator, receiver_network_operator=receiver)
            .set_dossierid(dossier_id)
            .set_currentnetworkoperator('TST02')
            .set_typeofnumber("1")
            .add_enum_deactivation_operator_seq()
                .set_profileid("PROF-12")
                .set_default_service("Y")
                .finish()
            .add_enum_deactivation_operator_seq()
                .set_profileid("PROF-32")
                .set_default_service("Y")
                .finish()
            .build()
        )
        print(enumdeactivationoperator)
        self._sender.send_message(enumdeactivationoperator)

    def enumactivationrange(self, dossier_id: str, number_start: str, number_end: str, receiver: str):
        enumactivationrange = (
            EnumActivationRangeBuilder()
            .set_header(sender_network_operator=self.operator, receiver_network_operator=receiver)
            .set_dossierid(dossier_id)
            .set_currentnetworkoperator('TST02')
            .set_typeofnumber("1")
            .add_enum_activation_range_seq()
                .set_number_series(number_start, number_end)
                .add_enum_profiles("PROF1", "PROF2")
                .finish()
            .build()
        )
        print(enumactivationrange)
        self._sender.send_message(enumactivationrange)

    def enumdeactivationrange(self, dossier_id: str, number_start: str, number_end: str, receiver: str):
        enumdeactivationrange = (
            EnumDeactivationRangeBuilder()
            .set_header(sender_network_operator=self.operator, receiver_network_operator=receiver)
            .set_dossierid(dossier_id)
            .set_currentnetworkoperator('TST02')
            .set_typeofnumber("1")
            .add_enum_deactivation_range_seq()
                .set_number_series(number_start, number_end)
                .add_enum_profiles("PROF1", "PROF2")
                .finish()
            .build()
        )
        print(enumdeactivationrange)
        self._sender.send_message(enumdeactivationrange)

    def test_enumprofileactivation(self, dossier_id: str):
        enumprofileactivation = (
            EnumProfileActivationBuilder()
            .set_header(sender_network_operator='TST01', receiver_network_operator='TST02')
            .set_dossierid(dossier_id)
            .set_currentnetworkoperator('TST02')
            .set_typeofnumber("3")
            .set_scope("SCOPE-123")
            .set_profileid("5879AF-GNU9480N")
            .set_ttl("7")
            .set_dnsclass("abc def")
            .set_rectype("uvw xyz")
            .set_order("13")
            .set_preference("13")
            .set_flags("abc, def")
            .set_enumservice("Enum Service 123")
            .set_regexp("[A-Z][a-z]+\\\\d")
            .set_usertag("tag123123kjkjk")
            .set_domain("domain123")
            .set_spcode("23kjkj324")
            .set_processtype("processTypeX")
            .set_gateway("6i3k")
            .set_service("sdf sdfdsf")
            .set_domaintag("fdksalfds2132")
            .set_replacement("sdf dsjhsfd sdfsfd")
            .build()
        )
        print(enumprofileactivation)
        self._sender.send_message(enumprofileactivation)

    def test_enumprofiledeactivation(self, dossier_id: str):
        enumprofiledeactivation = (
            EnumProfileDeactivationBuilder()
            .set_header(sender_network_operator='TST01', receiver_network_operator='TST02')
            .set_dossierid(dossier_id)
            .set_currentnetworkoperator('TST02')
            .set_typeofnumber("1")
            .set_profileid("PROF-12")
            .build()
        )
        print(enumprofiledeactivation)
        self._sender.send_message(enumprofiledeactivation)

    @staticmethod
    def _generate_random_dossier_id(operator: str):
        random_int = random.randint(10000, 99999)
        return f'{operator}-{random_int}'


class QuickstartReceiver(Receiver):
    def __init__(self, config: NpConfig):
        super().__init__(config)
        self._sender = Sender(config)

    def on_keep_alive(self, message_id):
        print('on keep alive')
        pass

    def on_porting_request(self, message_id, message):
        print('porting request')
        self.handle_message(message_id, message)

    def on_porting_request_answer(self, message_id, message):
        print('porting request answer')
        self.handle_message(message_id, message)

    def on_porting_request_answer_delayed(self, message_id, message):
        print('porting request answer delayed')
        self.handle_message(message_id, message)

    def on_porting_performed(self, message_id, message):
        print('porting performed')
        self.handle_message(message_id, message)

    def on_deactivation(self, message_id, message):
        print('deactivation')
        self.handle_message(message_id, message)

    def on_cancel(self, message_id, message):
        print('cancel')
        self.handle_message(message_id, message)

    def on_error_found(self, message_id, message):
        print('error!')
        self.handle_message(message_id, message)

    def on_activation_service_number(self, message_id, message):
        print('activation_service_number')
        self.handle_message(message_id, message)

    def on_deactivation_service_number(self, message_id, message):
        print('deactivation_service_number')
        self.handle_message(message_id, message)

    def on_tariff_change_service_number(self, message_id, message):
        print('tariff_change_service_number')
        self.handle_message(message_id, message)

    def on_range_activation(self, message_id, message):
        print('range activation')
        self.handle_message(message_id, message)

    def on_range_deactivation(self, message_id, message):
        print('range deactivation')
        self.handle_message(message_id, message)

    def on_enum_activation_number(self, message_id, message):
        print('enum activation number')
        self.handle_message(message_id, message)

    def on_enum_activation_range(self, message_id, message):
        print('enum activation range')
        self.handle_message(message_id, message)

    def on_enum_activation_operator(self, message_id, message):
        print('enum activation operator')
        self.handle_message(message_id, message)

    def on_enum_deactivation_number(self, message_id, message):
        print('enum deactivation number')
        self.handle_message(message_id, message)

    def on_enum_deactivation_range(self, message_id, message):
        print('enum deactivation range')
        self.handle_message(message_id, message)

    def on_enum_deactivation_operator(self, message_id, message):
        print('enum deactivation operator')
        self.handle_message(message_id, message)

    def on_enum_profile_activation(self, message_id, message):
        print('enum profile activation')
        self.handle_message(message_id, message)

    def on_enum_profile_deactivation(self, message_id, message):
        print('enum profile deactivation')
        self.handle_message(message_id, message)

    def handle_message(self, message_id, message):
        print(message)
        self._sender.confirm(message_id)


if __name__ == '__main__':
    config = NpConfig(
        'https://dev-api.coin.nl',
        'consumer-name',
        private_key_file='../../test/setup/private-key.pem',
        hmac_secret='../../test/setup/sharedkey.encrypted'
    )
    # For running locally against docker-compose
    # config = NpConfig(
    #     'http://localhost:8000',
    #     'loadtest-loada',
    #     private_key_file='../../test/setup/private-key.pem',
    #     hmac_secret='../../test/setup/sharedkey.encrypted'
    # )

    set_logging(level=logging.DEBUG)
    sender = QuickstartSender(config, 'TST01')
    sender.porting_request('TST01-0012012', '0303800007', '0303800007')
    # sender.porting_request_answer('TST01-0012012', '0303800007', '0303800007', 'TST02')
    # sender.porting_performed('TST01-0012012', '0303800007', '0303800007', 'TST02')
    # sender.deactivation('TST01-0012012', '0303800007', '0303800007', 'TST02')
    sender.activationsn('TST01-0012012', '0303800007', '0303800007', 'TST02')
    sender.deactivationsn('TST01-0012012', '0303800007', '0303800007', 'TST02')
    sender.tariffchangesn('TST01-0012012', '0303800007', '0303800007', 'TST02')
    sender.enumactivationnumber('TST01-0012012', '0303800007', '0303800007', 'TST02')
    sender.enumdeactivationnumber('TST01-0012012', '0303800007', '0303800007', 'TST02')
    sender.enumactivationoperator('TST01-0012012', 'TST02')
    sender.enumdeactivationoperator('TST01-0012012', 'TST02')
    sender.enumactivationrange('TST01-0012012', '0303800007', '0303800007', 'TST02')
    sender.enumdeactivationrange('TST01-0012012', '0303800007', '0303800007', 'TST02')

    # receiver = QuickstartReceiver(config)
    # receiver.start_stream()  # start_stream will block until interrupt or connection failure
    # receiver.start_stream(confirmation_status = ConfirmationStatus.UNCONFIRMED, message_types = [MessageType.PORTING_PERFORMED_V3]) # only listen for portingperformed messages
    # receiver.start_stream(offset = 1234, confirmation_status = ConfirmationStatus.ALL) # get all messages from offset 1234
